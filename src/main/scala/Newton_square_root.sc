// This worksheet is about recursive functions in scala. The sample
// application that will be used is finding square root of a number by
// newtons method.

/*
  Newtons method for finding square root of a number:
  http://en.wikipedia.org/wiki/Newton%27s_method
*/
// We will be using worksheet to maintain the latest optimized code..

// Newton method #1
/*
def sqrtIter(guess: Double, x: Double): Double = {if (isgoodenough(guess, x)) guess else sqrtIter(improve(guess, x), x)}

def isgoodenough(guess: Double, x: Double): Boolean = {math.abs(guess * guess - x) / x < 0.001}

def improve(guess: Double, x: Double) = {(guess + x / guess) / 2}

def sqroot(x: Int): Double = sqrtIter(1.0, x)

def main(arg: Array[String]) = {println("Squareroot is:" )}

sqroot(2)
*/

// Newton method #2
// One of the obvious fallback of above approach is, all the functions are visible openly, need to
// have a namespace. So we can rewrite them as:

/*
def sqroot(x:Int): Double = {

  def sqrtIter(guess: Double, x: Double): Double = {
    if (isgoodenough(guess, x)) guess else sqrtIter(improve(guess, x), x)
  }

  def isgoodenough(guess: Double, x: Double): Boolean = {
    math.abs(guess * guess - x) / x < 0.001
  }

  def improve(guess: Double, x: Double) = {
    (guess + x / guess) / 2
  }

  sqrtIter(1.0, x)
}
sqroot(3)
*/

// Newton method #3
// Since guess & x are used in all functions, we can eliminate them as well.

def sqroot(x:Int): Double = {

  def sqrtIter(guess: Double): Double = {
    if (isgoodenough(guess)) guess else sqrtIter(improve(guess))
  }

  def isgoodenough(guess: Double): Boolean = {
    math.abs(guess * guess - x) / x < 0.00001
  }

  def improve(guess: Double) = {
    (guess + x / guess) / 2
  }

  sqrtIter(1.0)
}
sqroot(4)