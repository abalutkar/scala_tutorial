/**
 * Created by Amar on 25/2/15.
 */

// You saw in main_1 how to run scala app.
// This example shows how to run app without using trait but defining a main function. Here, we can handle commandline
// arguments as well.

object main_2 {
  def main(args: Array[String]): Unit = {
    println("Hello World 2!");
  }
}
