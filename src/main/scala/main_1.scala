/**
 * Created by Amar on 25/2/15.
 */
// This class shows how to write a "scala app" i.e which can be run from console or eclipse/intellij
// For running from console run it as "scala 'objectname'"

// The object main_1 extends trait App. However this kindof call doesnt take commandline arguments. For passing
// commandline args refer main_2
object main_1 extends App{

    println("Hello World 1!");
}
