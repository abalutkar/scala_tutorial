// This is to check the evaluation strategies of Scala

//Call By Name
// This means, the assignment request is not evaluated immediately, however
//it gets evaluated once we are calling the resource.

def loop:Int = loop
// ^ Here we have defined loop as loop itself. However, this doesn't get evaluated
// till loop is called. Hence, this is call by name.

//Call By Value
val loop_cbv:Int = loop_cbv
// ^ This will never get computed, since the evaluation is done during initialization

// In scala, default function evaluation is done by CBV. Incase, we want function
//arguments to be evaluated by CBN, we use "=>"(like in functions)

def test_func(a:Int, b : => Int) {println("1st argument is:" + a)}

test_func(4, loop)

