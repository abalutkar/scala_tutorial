/**
 * Created by Amar on 25/2/15.
 */
object Newton_sqroot {
  def sqrtIter(guess: Double, x: Double): Double = {
    println("guess:" + guess + "x:" + x)
    if (isgoodenough(guess, x)) guess else sqrtIter(improve(guess, x), x)
  }

  def isgoodenough(guess: Double, x: Double): Boolean = {
    math.abs(guess * guess - x) / x < 0.001
  }

  def improve(guess: Double, x: Double) = {
    (guess + x / guess) / 2
  }

  def sqroot(x: Int): Double = sqrtIter(1.0, x)

  def main(arg: Array[String]) = {
    println(sqroot(2))
  }
}
